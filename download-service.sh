#!/usr/bin/env bash
set -eu -o pipefail

if [ "$#" != 3 ]; then
  echo "Usage: download-service.sh [https://gitlab.com/account/repo.git] [dest] [checkout ref, e.g., master]"
  exit 1
fi

REPO="$1"
DEST="$2"
REF="$3"

# $GITLAB_USER $GITLAB_TOKEN stored in LayerCI parameters store
CLONE_URL="https://$GITLAB_USER:$GITLAB_TOKEN@gitlab.com/$REPO"

if [ -d "$DEST" ]; then
  cd "$DEST"
  git remote remove origin 2>/dev/null || true
else
  rm -rf "$DEST"
  mkdir -p "$DEST"
  cd "$DEST"
  git init
fi

git remote add origin "$CLONE_URL"
git fetch
git reset --hard "$REF"
git submodule update --init --remote --recursive
git clean -f -d

echo "[download-service.sh]: Successfully checked out ref $REF in $REPO"
echo "[download-service.sh]: Destination: $DEST"