#!/usr/bin/env bash
set -eu -o pipefail

if [ "$#" != 1 ]; then
  echo "Usage: build_config.sh [build subdomain, e.g., layer-api, layer-admin, layer-ssr]"
  exit 1
fi

# Extract base env values from parameters store
echo "$API_ENV" | base64 -d > ./api/.env
echo "$SOCKET_ENV" | base64 -d > ./socket/.env
echo "$SSR_ENV" | base64 -d > ./ssr/.env

# Example - https://master.layer-api.vomapay.com
BUILD_SUBDOMAIN="$1"
BUILD_HOST=https://$SUBDOMAIN.$BUILD_SUBDOMAIN.vomapay.com

# Update host based values
echo "
APP_URL=$BUILD_HOST
APP_FRONTEND_URL=$BUILD_HOST
AWS_ACCESS_KEY_ID=$AWS_RUN_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY=$AWS_RUN_SECRET_ACCESS_KEY
PAYPAL_WEBHOOK_URL=$BUILD_HOST/paypal/webhook
APP_PASSWORDLESS_URL=https://:account.$SUBDOMAIN.$BUILD_SUBDOMAIN.vomapay.com/update
FACEBOOK_REDIRECT_URI=$BUILD_HOST/integrations
" >> ./api/.env

# Host based SSR env values
echo "
ADMIN_HOST=$BUILD_HOST
PURCHASE_HOST=$SUBDOMAIN.$BUILD_SUBDOMAIN.vomapay.com
SSR_HOST=$BUILD_HOST
API=$BUILD_HOST/api-layer/v1
API_INTERNAL=$BUILD_HOST/api-layer/v1
SOCKET_IO_ENDPOINT=$BUILD_HOST/socket-io
AWS_ACCESS_KEY_ID=$AWS_RUN_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY=$AWS_RUN_SECRET_ACCESS_KEY
" >> ./ssr/.env

#HOST based ADMIN env values
echo "
APP_ENV=layer
API_HOST=$SUBDOMAIN.$BUILD_SUBDOMAIN.vomapay.com
API_ENDPOINT=$BUILD_HOST/api-layer/v1
SOCKET_IO_ENDPOINT=$BUILD_HOST/socket-io
IMAGE_UPLOAD_URL=$BUILD_HOST/api-layer/v1/images
PURCHASE_HOST=$SUBDOMAIN.$BUILD_SUBDOMAIN.vomapay.com
LAYER_INTEGRATION_REDIRECT_ORIGIN=https://layer-oauth-target.$BUILD_SUBDOMAIN.vomapay.com
" >> ./admin/.env.development

sed -i "s|\${BUILD_HOST}|$SUBDOMAIN.$BUILD_SUBDOMAIN.vomapay.com|" ./admin/docker/nginx/layer-ci.conf